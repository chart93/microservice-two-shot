from django.shortcuts import render
from .models import Hat
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import LocationVO
# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        "section_number",
        "shelf_number",
        ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'hat_id',
        "fabric",
        "style_name",
        "color",
        ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "hat_id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST", "DELETE"])
def hat_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        location = LocationVO.objects.get(import_href=content["location"])
        content["location"] = location
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def hat_detail(request, hat_id):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(hat_id=hat_id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "GET":
        hat = Hat.objects.get(hat_id=hat_id)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        Hat.objects.filter(hat_id=hat_id).update(**content)
        hat = Hat.objects.get(hat_id=hat_id)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )
