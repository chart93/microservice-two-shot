import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO


def get_bin():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    # make request to server for information
    content = json.loads(response.content)
    # make value object for each one of those resources
    # content = response.json()
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={
                "bin_number": bin["bin_number"]
                #   "closet_name": bin["closet_name"]
            },
        )


def poll():
    while True:
        try:
            print("shoe's poller polling for data")
            get_bin()
        except Exception as e:
            print(e)
        time.sleep(60)


if __name__ == "__main__":
    poll()
