# Generated by Django 4.0.3 on 2024-05-01 02:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=200, unique=True)),
                ('bin_number', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Shoes',
            fields=[
                ('manufacturer', models.CharField(max_length=200)),
                ('model_name', models.CharField(max_length=200)),
                ('color', models.CharField(max_length=200)),
                ('picture_url', models.URLField(null=True)),
                ('shoe_id', models.PositiveIntegerField(primary_key=True, serialize=False)),
                ('bin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='shoe_bin', to='shoes_rest.binvo')),
            ],
        ),
    ]
