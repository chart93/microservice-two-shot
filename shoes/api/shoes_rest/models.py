from django.db import models
from django.urls import reverse

# from wardrobe.api.wardrobe_api.models import Bin


# Create your models here.


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.PositiveSmallIntegerField()


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    shoe_id = models.PositiveIntegerField(primary_key=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoe_bin",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return self.shoe_id


# class Bin(models.Model):
#     closet_name = models.CharField(max_length=100)
#     bin_number = models.PositiveSmallIntegerField()
#     bin_size = models.PositiveSmallIntegerField()

#     def get_api_url(self):
#         return reverse("api_bin", kwargs={"pk": self.pk})

#     def __str__(self):
#         return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

#     class Meta:
#         ordering = ("closet_name", "bin_number", "bin_size")
