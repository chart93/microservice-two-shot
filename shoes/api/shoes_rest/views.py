from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json


from .models import Shoes, BinVO

from common.json import ModelEncoder

# from wardrobe.api.wardrobe_api.views import Bin

# Create your views here.


# THESE JSON ENCODERS HAVE NOTHING TO DO WITH INFORMATION BEING SENT TO THE SERVER


# ENCODERS SEND DATA BACK IN THE RESPONSE IN INSOMNIA SO WE CAN VIEW THEM. THEY DO NOT MAKE IT READIBLE TO THE DATABASE
# WHAT WE SEE IN INSOMNIA IS WHAT REACT SEES
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "bin_number",
        "import_href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "shoe_id",
        "bin",
    ]
    encoders = {"bin": BinVOEncoder()}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "shoe_id",
        "bin",
    ]
    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoe(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        # MAKES CONTENT READAABLE TO DATABASE EXCEPT FOREIGN KEY
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            # THIS ALLOWS US TO GRAB THE FOREIGN KEY DATA/CONTENT
            # ^ this defines request body (http request(insomina) - post request bin key)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid bin"})

        shoe = Shoes.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "DELETE":
        count, _ = Shoes.objects.filter(shoe_id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        shoe = Shoes.objects.get(shoe_id=pk)
        return JsonResponse({"shoe": shoe}, encoder=ShoeDetailEncoder)


# DELETE
# elif request.method == "DELETE":
#         count, _ = Shoes.objects.filter(shoe_id=pk).delete()
#         return JsonResponse({"deleted": count > 0})

# shoes = Shoes.objects.get(shoe_id=pk)
