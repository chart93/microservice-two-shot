import React, { useEffect, useState } from 'react';



function ShoesList(props) {

    const [shoes, setShoe] = useState([]);

    async function getShoes() {
        const locationUrl = 'http://localhost:8080/api/shoes/'
        const response = await fetch(locationUrl)
        if (response.ok) {
            const { shoes } = await response.json();
            setShoe(shoes)
        } else {
            console.error('Error')
        }
    }

    // GRAB ALL THE DATA FROM THE SERVER AND MAP THROUGH IT IN YOUR JSX

    // string literals HAVE BACKTICKS `````````NOT ''''''''
    useEffect(() => {
        getShoes();
    }, []);

    // ASYNC LETS JAVASCRIPT KNOW THAT YOU HAVE SOMETHING IN YOUR FUNCTION THAT MIGHT TAKE A WHILE, SO WAIT UNTIL IT GRABS IT TO PROCEED RUNNING THE FUNCTION
    const onDelete = async (href) => {
        if (window.confirm("Do you want to remove?")) {
            fetch("http://localhost:8080" + href,
                { method: "DELETE" }).then(() => {
                    window.location.reload();
                }).catch((err) => {
                    console.log(err.message)

                })
        }
    }

    return (
        <div className="shoe-list">
            <div className="shoe-details" key={shoes.href}>
                < table className="table table-striped" >
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                            <th>Model</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Shoe ID</th>
                            <th>Bin Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {shoes.map((shoe) => {
                            console.log(shoe)
                            return (
                                <tr>
                                    <td>{shoe.manufacturer}</td>
                                    <td>{shoe.model_name}</td>
                                    <td>{shoe.color}</td>
                                    <td><img src={shoe.picture_url} alt={shoe.model_name} style={{ width: '100px', height: '100px' }} /> </td>
                                    {/* google html image to figure out how to make sure image shows */}
                                    <td>{shoe.shoe_id}</td>
                                    <td>{shoe.bin.bin_number}</td>
                                    <td><button onClick={() => onDelete(shoe.href)} >Delete</button></td>
                                    {/* google how to do onclick delete button. Need to onclick and make a fetch call INSIDE of an onclick to "delete" */}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div >
    )
}

export default ShoesList;




// return (
//     < table className="table table-striped" >
//         <thead>
//             <tr>
//                 <th>Manufacturer</th>
//                 <th>Model Name</th>
//                 <th>Picture URL</th>
//                 <th>Shoe ID</th>
//                 <th>Bin</th>
//             </tr>
//         </thead>
//         <tbody>
//             {/* for (let attendee of props.attendees) {
//   <tr>
//     <td>{ attendee.name }</td>
//     <td>{ attendee.conference }</td>
//   </tr>
// } */}
//             {props.shoes.map(shoe => {
//                 return (
//                     <tr key={shoe.href}>
//                         <td>{shoe.manufacturer}</td>
//                         <td>{shoe.model_name}</td>
//                         <td>{shoe.picture_url}</td>
//                         <td>{shoe.shoe_id}</td>
//                         <td>{shoe.bin}</td>
//                     </tr>
//                 );
//             })}
//         </tbody>
//     </table >
// )
// }
