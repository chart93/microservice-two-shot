import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatList from './HatList'
import ShoeForm from './CreateShoeForm'
import ShoesList from './ShoesList'

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatForm/>}/>
          <Route path="/hats/list" element={<HatList/>}/>
          <Route path="/new" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeForm />} />
          <Route path="/shoes/list" element={<ShoesList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
