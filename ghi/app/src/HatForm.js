import React, { useEffect, useState } from 'react';

function HatForm () {
  const [locations, setLocations] = useState([])
  const [HatData, setHatData] = useState(
    {
      hat_id: '',
      fabric: '',
      style_name: '',
      color: '',
      picture_url: '',
      location: ''
  })

  const fetchData = async () => {
    const locationUrl = 'http://localhost:8100/api/locations/';
    const response = await fetch(locationUrl);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, [])


  const handleSubmit = async (event) => {
    event.preventDefault()

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
    method: "post",
    body: JSON.stringify(HatData),
    headers: {
      'Content-Type': 'application/json',
    },
  };

    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      // const newHat = await response.json();
      // console.log(newHat);

      setHatData(
      {
        hat_id: '',
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: ''
        }
      )
  }
  }



  const handleHatChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setHatData({
      ...HatData,
      [inputName]: value
    })
  }


    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleHatChange} placeholder="Hat ID" required type="text" name="hat_id" id="hat_id" className="form-control"/>
              <label for="hat_id">Hat ID</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleHatChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
              <label for="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleHatChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
              <label for="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleHatChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
              <label for="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleHatChange} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control"/>
              <label for="picture">Picture</label>
            </div>
            <div className="mb-3">
              <select onChange={handleHatChange} required name="location" id="location" className="form-select">
                <option selected value="">Choose a location</option>
                {locations.map(location => {
                return (
                <option key={location.closet_name} value={location.href}>
                  {location.closet_name}</option>
                )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    );
}

export default HatForm;
