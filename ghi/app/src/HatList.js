import React, { useEffect, useState } from 'react';

function HatList(){
  const [hats, setHats] = useState([])

  const getHats = async () => {
    const response = await fetch('http://localhost:8090/api/hats/')

    if (response.ok){
      const data = await response.json()
      setHats(data.hats)
    }
  }

  // const handleDelete = (hatToDelete) =>{
  //   const newHats = hats.filter((hat_id) => hat_id!== hatToDelete)
  //   setHats(newHats)
  // }
  const handleDelete = async (href) => {
    if (window.confirm("Do you want to delete?")){
      fetch("http://localhost:8090/" + href,
        { method: "DELETE"}).then(() =>{
          window.location.reload();
        }).catch((err) => {
          console.log(err.message)
        })
    }
  }


  useEffect(() => {
    getHats()
  }, [])

    return(
        <table className="table table-stripped">
      <thead>
        <tr>
        <th>Delete</th>
          <th>Hat ID</th>
          <th>Fabric</th>
          <th>Style</th>
          {/* <th>Location</th> */}
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.href}>
              <td><button onClick={() => handleDelete(hat.href)}>Delete</button></td>
              <td>{ hat.hat_id }</td>
              <td>{ hat.fabric }</td>
              <td>{ hat.style_name }</td>
              {/* <td>{ hat.location.closet_name }</td> */}
              <td><img className='image' src={hat.picture_url} alt='hat image'></img> </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    )
  }

export default HatList
