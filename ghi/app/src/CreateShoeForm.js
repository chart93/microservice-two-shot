import React, { useEffect, useState } from 'react';
import ShoesList from './ShoesList'

function ShoeForm() {

    const [bins, setBins] = useState([]);
    const [chooseBin, setChooseBin] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [shoeId, setShoeId] = useState('');

    const handleChooseBin = (event) => {
        const value = event.target.value;
        setChooseBin(value)
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    }

    const handleModelName = (event) => {
        const value = event.target.value;
        setModelName(value)
    }

    const handlePictureUrl = (event) => {
        const value = event.target.value;
        setPictureUrl(value)
    }

    const handleShoeId = (event) => {
        const value = event.target.value;
        setShoeId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            // bin = chooseBin (can do it this way for alternate formatting)
        };
        data.bin = chooseBin;
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.picture_url = pictureUrl;
        data.shoe_id = shoeId
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {

            // const newShoe = await response.json();
            // console.log(newShoe)

            setChooseBin('');
            setManufacturer('');
            setModelName('');
            setPictureUrl('');
            setShoeId('');

        }

    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    // ShoesList shoes = { data}

    useEffect(() => {
        fetchData();
    }, []);


    // if you set values to react values then you can modify and change
    return (<div className="container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe!</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturer} value={manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                            <label htmlFor="name">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelName} value={modelName} placeholder="Start date" required type="text" id="model" name="model" className="form-control" />
                            <label htmlFor="starts">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrl} value={pictureUrl} placeholder="Picture Url" required type="text" id="picture" name="picture" className="form-control" />
                            <label htmlFor="ends">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleShoeId} value={shoeId} placeholder="Shoe Id" required type="number" id="shoe_id" name="shoe_id" className="form-control" />
                            <label htmlFor="shoe_id">Shoe ID</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="chooseBin" className="form-label">Bin</label>
                            <select onChange={handleChooseBin} required id="chooseBin" name="chooseBin" className="form-select">
                                <option value="">Choose a Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {/* KEY anytime you use a map you need a key so react can keep track of data (we do not use it). It needs to be a unique identifier */}
                                            {/* VALUE is what we use in event.target.value */}
                                            {/* backend searches for state by abbreviation of that state. all this info is to create a post request.*/}
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        {/* default for button inside of a form is to submit */}
                    </form>
                </div>
            </div>
        </div>
    </div>
    )

}

export default ShoeForm;
